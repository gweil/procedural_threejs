import * as THREE from 'three';
import { OrbitControls } from 'three/addons/controls/OrbitControls.js';

let iw = window.innerWidth;
let ih = window.innerHeight;

const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera(75, iw/ih, 0.1, 10000);
const renderer = new THREE.WebGLRenderer({canvas});
const canvasDOM = renderer.domElement;
const controls = new OrbitControls(camera, canvasDOM);
const clock = new THREE.Clock();


renderer.setPixelRatio(window.devicePixelRatio);
renderer.setSize(iw, ih);
camera.position.y = 1;
camera.position.z = 1;


// Generate particles
function generateParticles(nbParticles) {
    const vertices = [];
    const colors = [];
    let color = new THREE.Color();
    for (let i = 0; i < nbParticles; i++) {
        // position
        vertices.push(Math.random() * 2 - 1);
        vertices.push(0);
        vertices.push(Math.random() * 2 - 1);
        // color
        color.setHSL(Math.random(), 1, 0.5);
        colors.push(color.r);
        colors.push(color.g);
        colors.push(color.b);
    }

    const particlesCoords = new THREE.BufferGeometry();
    particlesCoords.setAttribute('position', new THREE.Float32BufferAttribute(vertices, 3));
    particlesCoords.setAttribute('color', new THREE.Float32BufferAttribute(colors, 3));
    
    const material = new THREE.PointsMaterial({
        vertexColors: true,
        size: 0.005
    });

    const particles = new THREE.Points(particlesCoords, material);
    particles.name = "particles";
    scene.add(particles);
}

function updateParticlesPosition() {
    const tick = clock.getElapsedTime()/4;
    const particles = scene.getObjectByName("particles");
    const nbParticles = particles.geometry.attributes.position.count;
    const posArray = particles.geometry.getAttribute('position');

    for (let i = 0; i < nbParticles; i++) {
        let x = posArray.getX(i);
        posArray.setY(i, Math.sin((x+1+tick)*Math.PI)/4);
    }
    
    particles.geometry.setAttribute('position', posArray);
    posArray.needsUpdate = true;
}

function animate() {
	requestAnimationFrame(animate);
    updateParticlesPosition();
    controls.update();
	renderer.render(scene, camera);
}

window.onresize = function updateSize() {
    iw = window.innerWidth;
    ih = window.innerHeight;
    renderer.setSize(iw, ih);
    camera.aspect = canvasDOM.clientWidth / canvasDOM.clientHeight;
    camera.updateProjectionMatrix();
};

generateParticles(100000);
animate();